# Planetarium API
API for booking tickets online for show in Planetarium.

## Features:
- Login/Register user
- Crete/Remove reservations and tickets
- Create/Update/Remove show sessions, astronomy shows, show themes, planetarium domes (only for admin)
- List tickets in order

## Build and run
```bash
$ git clone git@gitlab.com:lotrikys/planetariumapi.git
$ python3 -m venv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
$ python3 manage.py migrate
$ python3 manage.py createsuperuser
$ python3 manage.py runserver
```
For production use you need to define variable SECRET_KEY by using environment variables, .env or settings.ini files.

Environment variables example:
```bash
$ export SECRET_KEY=<your_secret_key>
```

.env example:
```text
SECRET_KEY=<your_secret_key>
```

settings.ini example:
```text
[settings]
SECRET_KEY=<your_secret_key>
