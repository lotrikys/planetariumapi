from django.urls import path, include
from rest_framework import routers

from main.views import (
    ShowSessionViewSet,
    AstronomyShowViewSet,
    ShowThemeViewSet,
    PlanetariumDomeViewSet,
    TicketViewSet,
    ReservationViewSet,
)

router = routers.DefaultRouter()
router.register("show_sessions", ShowSessionViewSet)
router.register("astronomy_shows", AstronomyShowViewSet)
router.register("show_themes", ShowThemeViewSet)
router.register("planetarium_domes", PlanetariumDomeViewSet)
router.register("tickets", TicketViewSet)
router.register("reservations", ReservationViewSet)

urlpatterns = [
    path("", include(router.urls)),
]

app_name = "main"
