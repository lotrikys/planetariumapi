from django.db.models import F, Count
from rest_framework import viewsets
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated

from main.models import (
    ShowSessionModel,
    AstronomyShowModel,
    ShowThemeModel,
    PlanetariumDomeModel,
    TicketModel,
    ReservationModel,
)
from main.permissions import IsAdminOrIfAuthenticatedReadOnly
from main.serializers import (
    ShowSessionSerializer,
    AstronomyShowSerializer,
    ShowThemeSerializer,
    PlanetariumDomeSerializer,
    ShowSessionListSerializer,
    AstronomyShowListSerializer,
    TicketSerializer,
    ReservationSerializer,
    ReservationListSerializer,
    TicketListSerializer,
    ReservationDetailSerializer,
)


class ShowSessionViewSet(viewsets.ModelViewSet):
    serializer_class = ShowSessionSerializer
    queryset = ShowSessionModel.objects.select_related(
        "astronomy_show", "planetarium_dome"
    )
    permission_classes = (IsAdminOrIfAuthenticatedReadOnly,)

    def get_serializer_class(self):
        if self.action in ("list", "retrieve"):
            return ShowSessionListSerializer
        return ShowSessionSerializer

    def get_queryset(self):
        queryset = self.queryset.annotate(
            tickets_available=F("planetarium_dome__rows")
            * F("planetarium_dome__seats_in_row")
            - Count("tickets")
        )
        return queryset


class AstronomyShowViewSet(viewsets.ModelViewSet):
    serializer_class = AstronomyShowSerializer
    queryset = AstronomyShowModel.objects.prefetch_related("show_theme")
    permission_classes = (IsAdminOrIfAuthenticatedReadOnly,)

    def get_serializer_class(self):
        if self.action in ("list", "retrieve"):
            return AstronomyShowListSerializer
        return AstronomyShowSerializer


class ShowThemeViewSet(viewsets.ModelViewSet):
    serializer_class = ShowThemeSerializer
    queryset = ShowThemeModel.objects.all()
    permission_classes = (IsAdminOrIfAuthenticatedReadOnly,)


class PlanetariumDomeViewSet(viewsets.ModelViewSet):
    serializer_class = PlanetariumDomeSerializer
    queryset = PlanetariumDomeModel.objects.all()
    permission_classes = (IsAdminOrIfAuthenticatedReadOnly,)


class TicketViewSet(viewsets.ModelViewSet):
    serializer_class = TicketSerializer
    queryset = TicketModel.objects.select_related(
        "show_session", "reservation"
    )
    permission_classes = (IsAuthenticated,)

    def get_serializer_class(self):
        if self.action in ("list", "retrieve"):
            return TicketListSerializer
        return TicketSerializer

    def get_queryset(self):
        return self.queryset.filter(
            reservation__user=self.request.user
        ).order_by("id")

    def perform_create(self, serializer):
        user_id = ReservationModel.objects.get(
            id=self.request.data.get("reservation")
        ).user_id
        if self.request.user.id == user_id:
            serializer.save()
        else:
            raise ValidationError(
                "You cannot create ticket with reservation from another user"
            )


class ReservationViewSet(viewsets.ModelViewSet):
    serializer_class = ReservationSerializer
    queryset = ReservationModel.objects.all()
    permission_classes = (IsAuthenticated,)

    def get_serializer_class(self):
        if self.action == "list":
            return ReservationListSerializer
        if self.action == "retrieve":
            return ReservationDetailSerializer
        return ReservationSerializer

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
