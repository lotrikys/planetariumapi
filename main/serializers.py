from rest_framework import serializers

from main.models import (
    ShowSessionModel,
    AstronomyShowModel,
    ShowThemeModel,
    PlanetariumDomeModel,
    TicketModel,
    ReservationModel,
)


class ShowSessionListSerializer(serializers.ModelSerializer):
    astronomy_show = serializers.CharField(source="astronomy_show.title")
    planetarium_dome = serializers.CharField(source="planetarium_dome.name")
    tickets_available = serializers.IntegerField(read_only=True)

    class Meta:
        model = ShowSessionModel
        fields = (
            "id",
            "astronomy_show",
            "planetarium_dome",
            "show_time",
            "tickets_available",
        )


class ShowSessionSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShowSessionModel
        fields = ("id", "astronomy_show", "planetarium_dome", "show_time")

    def update(self, instance, validated_data):
        instance.astronomy_show = validated_data.get(
            "astronomy_show", instance.astronomy_show
        )
        instance.planetarium_dome = validated_data.get(
            "planetarium_dome", instance.planetarium_dome
        )
        instance.show_time = validated_data.get(
            "show_time", instance.show_time
        )
        instance.save()
        return instance


class AstronomyShowListSerializer(serializers.ModelSerializer):
    show_theme = serializers.SlugRelatedField(
        many=True, read_only=True, slug_field="name"
    )

    class Meta:
        model = AstronomyShowModel
        fields = ("id", "title", "description", "show_theme")


class AstronomyShowSerializer(serializers.ModelSerializer):
    class Meta:
        model = AstronomyShowModel
        fields = ("id", "title", "description", "show_theme")

    def update(self, instance, validated_data):
        instance.title = validated_data.get("title", instance.title)
        instance.description = validated_data.get(
            "description", instance.description
        )
        instance.show_theme = validated_data.get(
            "show_theme", instance.show_theme
        )


class ShowThemeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShowThemeModel
        fields = ("id", "name")


class PlanetariumDomeSerializer(serializers.ModelSerializer):
    class Meta:
        model = PlanetariumDomeModel
        fields = ("id", "name", "rows", "seats_in_row")


class ReservationListSerializer(serializers.ModelSerializer):
    user = serializers.CharField(source="user.email")

    class Meta:
        model = ReservationModel
        fields = ("id", "created_at", "user")


class TicketDetailSerializer(serializers.ModelSerializer):
    show_session = ShowSessionListSerializer(many=False)

    class Meta:
        model = TicketModel
        fields = ("id", "row", "seat", "show_session")


class ReservationDetailSerializer(serializers.ModelSerializer):
    user = serializers.CharField(source="user.email")
    tickets = TicketDetailSerializer(many=True)

    class Meta:
        model = ReservationModel
        fields = ("id", "created_at", "user", "tickets")


class ReservationSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReservationModel
        fields = ("id", "created_at", "user")


class TicketListSerializer(serializers.ModelSerializer):
    show_session = ShowSessionListSerializer(many=False)
    reservation = ReservationListSerializer(many=False)

    class Meta:
        model = TicketModel
        fields = ("id", "row", "seat", "show_session", "reservation")


class TicketSerializer(serializers.ModelSerializer):
    class Meta:
        model = TicketModel
        fields = ("id", "row", "seat", "show_session", "reservation")
