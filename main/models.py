from django.core.exceptions import ValidationError
from django.db import models

from user.models import User


class ShowSessionModel(models.Model):
    astronomy_show = models.ForeignKey(
        "AstronomyShowModel",
        related_name="show_sessions",
        on_delete=models.CASCADE,
    )
    planetarium_dome = models.ForeignKey(
        "PlanetariumDomeModel",
        related_name="show_sessions",
        on_delete=models.CASCADE,
    )
    show_time = models.DateTimeField()

    def __str__(self):
        return f"Show {self.astronomy_show.title} in {self.planetarium_dome.name} at {self.show_time}"


class AstronomyShowModel(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    show_theme = models.ManyToManyField(
        "ShowThemeModel", related_name="astronomy_shows"
    )

    def __str__(self):
        return self.title


class ShowThemeModel(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class PlanetariumDomeModel(models.Model):
    name = models.CharField(max_length=255)
    rows = models.IntegerField()
    seats_in_row = models.IntegerField()

    def __str__(self):
        return self.name


class TicketModel(models.Model):
    row = models.IntegerField()
    seat = models.IntegerField()
    show_session = models.ForeignKey(
        ShowSessionModel, related_name="tickets", on_delete=models.CASCADE
    )
    reservation = models.ForeignKey(
        "ReservationModel", related_name="tickets", on_delete=models.CASCADE
    )

    class Meta:
        unique_together = ("show_session", "row", "seat")

    def clean(self):
        for (
            ticket_attr_value,
            ticket_attr_name,
            planetarium_dome_attr_name,
        ) in [
            (self.row, "row", "rows"),
            (self.seat, "seat", "seats_in_row"),
        ]:
            count_attrs = getattr(
                self.show_session.planetarium_dome, planetarium_dome_attr_name
            )
            if not (1 <= ticket_attr_value <= count_attrs):
                raise ValidationError(
                    {
                        ticket_attr_name: f"{ticket_attr_name} "
                        f"number must be in available range: "
                        f"from 1 to {count_attrs}"
                    }
                )

    def save(
        self,
        force_insert=False,
        force_update=False,
        using=None,
        update_fields=None,
    ):
        self.full_clean()
        super(TicketModel, self).save(
            force_insert, force_update, using, update_fields
        )


class ReservationModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(
        User, related_name="reservations", on_delete=models.CASCADE
    )

    def __str__(self):
        return f"Reservation made by {self.user.email} at {self.created_at}"
